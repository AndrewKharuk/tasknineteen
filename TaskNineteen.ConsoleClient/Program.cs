﻿using System;
using TaskNineteen.MotivatorLibrary;

namespace TaskNineteen.ConsoleClient
{
    class Program
    {
        static IMotivatorService motivator;
        static MotivatorProperty property;

        static void Main(string[] args)
        {
            motivator = new MotivatorService();

            property = new MotivatorProperty
            {
                MotivatorsDirectory = @"..\..\motivators\",
                PicturePath = @"..\..\pictures\28e15a00[1].jpg",
                MotivatorText = "Hello World!"
            };

            string path = motivator.CreateMotivator(property);

            Console.WriteLine($"Motivator created - path: {path}");

            Console.ReadKey();
        }
    }
}
