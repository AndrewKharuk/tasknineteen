﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskNineteen.MotivatorLibrary
{
    public interface IMotivatorService
    {
        string CreateMotivator(MotivatorProperty property);
    }
}
