﻿using System.Drawing;

namespace TaskNineteen.MotivatorLibrary
{
    public interface IMotivatorsCollector
    {
        void CompleteMotivatorAssembly(Bitmap background, Bitmap picture, string text);
    }
}
