﻿using System.Drawing;

namespace TaskNineteen.MotivatorLibrary
{
    public interface IImageMaster
    {
        Bitmap GetImageInSize(string picturePath);
        string GetImageName();
    }
}
