﻿using System;
using System.Drawing;

namespace TaskNineteen.MotivatorLibrary
{
    public class ImageMaster : IImageMaster
    {
        public Bitmap GetImageInSize(string picturePath)
        {
            var picture = new Bitmap(picturePath);
            Size s = picture.Size;
            s.Width = 256;
            s.Height = 256;
            return new Bitmap(Image.FromFile(picturePath), s);
        }

        public string GetImageName()
        {
            return $"Motivator_{DateTime.Now.ToShortDateString()}_{GetTime()}.jpg";
        }

        private string GetTime()
        {
            var time = DateTime.Now.ToLongTimeString();
          
            return time.Replace(':', '-'); ;
        }
    }
}
