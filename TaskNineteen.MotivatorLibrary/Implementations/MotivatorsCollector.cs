﻿using System.Drawing;

namespace TaskNineteen.MotivatorLibrary
{
    public class MotivatorsCollector : IMotivatorsCollector
    {
        public void CompleteMotivatorAssembly(Bitmap background, Bitmap picture, string text)
        {
            using (var g = Graphics.FromImage(background))
            {
                g.Clear(Color.DarkGreen);

                g.DrawString(text,
                                new Font("Verdana", (float)20),
                                new SolidBrush(Color.White),
                                new Rectangle(0, 0, 500, 100),
                                new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });

                g.DrawImage(picture, new Rectangle(122, 122, picture.Width, picture.Height));

                g.DrawLines(new Pen(Color.White, 4),
                            new PointF[] { new PointF(118.0F,  118.0F),
                                             new PointF(118.0F, 382.0F),
                                             new PointF(382.0F,  382.0F),
                                             new PointF(382.0F, 118.0F),
                                             new PointF(118.0F,  118.0F)
                            });
            }
        }
    }
}
