﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace TaskNineteen.MotivatorLibrary
{
    public class MotivatorService : IMotivatorService
    {
        IImageMaster _imageMaster;
        IMotivatorsCollector _motivatorsCollector;

        public MotivatorService()
        {
            _imageMaster = new ImageMaster();
            _motivatorsCollector = new MotivatorsCollector();
        }

        public string CreateMotivator(MotivatorProperty property)
        {
            var background = new Bitmap(500, 500);

            Bitmap picture = _imageMaster.GetImageInSize(property.PicturePath);

            string motivatorName = _imageMaster.GetImageName();

            _motivatorsCollector.CompleteMotivatorAssembly(background, picture, property.MotivatorText);

            string motivatorLocalPath = Path.Combine(property.MotivatorsDirectory, motivatorName);

            background.Save(motivatorLocalPath, ImageFormat.Jpeg);

            return Path.GetFullPath(motivatorLocalPath);
        }
    }
}

