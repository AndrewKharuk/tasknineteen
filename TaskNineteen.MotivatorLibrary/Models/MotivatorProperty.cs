﻿namespace TaskNineteen.MotivatorLibrary
{
    public class MotivatorProperty
    {
        public string MotivatorsDirectory { get; set; }
        public string PicturePath { get; set; }
        public string MotivatorText { get; set; }
    }
}
